<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'university');

/** MySQL database username */
define('DB_USER', 'university');

/** MySQL database password */
define('DB_PASSWORD', 'cityUniverse');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'uqC!,oQFD9G%hwp+n#Uf:iAvn3xv09$3+;~xK~t<7S`mo A<o>$ [)0vsHy}87aq');
define('SECURE_AUTH_KEY',  ']VcmB7]UEb9Un`79@fv$s8ykJ8Ck#LJx`2~%FCqxfn>[ey%9kT+2$3Zab4^=}kqB');
define('LOGGED_IN_KEY',    'PHQ@X%A&d][vjv+3s@_fG.l>H0X/PZMv8Kk{`Rhho&V.ksJL6M*AR%uhLcGNm/y$');
define('NONCE_KEY',        '9qsGMXLQ/?/t1TxT}#SK<|4^[3POt3u*7F6tkFt-F7y_G~YQxg28jIvfGIDM|ucm');
define('AUTH_SALT',        '2aWe|sc8&/aYK);L*sYBD&Ln`#N<_6=e^<6x/US_V8_o_7fwFNrh|gJaN30l+^}E');
define('SECURE_AUTH_SALT', '^-N5*5PXDAT;Aqqlrz54  CXXvn{w2qZ?Y{?QV,*5&<%&Qyk?tW?_oXGu A!DYu}');
define('LOGGED_IN_SALT',   'F8}! ,Ynn*p%.Y|AMoC}myi)ne$cg(<ko97Wp%9go7B)]lI+v+K3b7+BT2{c,g*P');
define('NONCE_SALT',       '[X8GtitOk~&Mi Uui0-pS 5a<a/^%)P&_>K!Po2Uwyt?^|M]+xM6-!-}b)5#QA6K');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
