<?php
/**
 * Template Name: Faculty Memners Template
 *
 * Description: A page template that provides a key component of WordPress as a CMS
 * by meeting the need for a carefully crafted introductory page. The front page template
 * in Twenty Twelve consists of a page content area for adding text, images, video --
 * anything you'd like -- followed by front-page-only widgets in one or two columns.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>


<div class="page_banner">
	<div class="banner_inner">
		
		<?php if (get_field('banner_image')) {
		?>
			 
		<?php
		}
		else{
		?>
			<img src=" <?php the_field('background_imagese', 'option'); ?>">
		<?php	
		}
		?>

		<div class="container">
			<div class="caption">
				<div class="bannerfirst_title-outer"><div class="bannerfirst_title"><?php the_field('faculty_title_first', 'option'); ?></div></div>
				<div class="bannersecond_title_outer"><div class="bannersecond_title"><?php the_field('faculty_second_first', 'option'); ?></div></div>
				<div class="bannersub_title_outer"><div class="bannersub_title"><?php the_field('faculty_sub_first', 'option'); ?></div></div>
				<div class="bannerextra_content"><?php the_field('add_extra_content'); ?></div>
			</div>
			<div class="faculty_member_tab">
				<ul  class="nav nav-pills">
					<li class="active">
		       			 <a  href="#1a" data-toggle="tab" class="sc_icon">SCIENCE & ENGINEERING</a>
					</li>
					<li class="second_t">
						<a href="#2a" data-toggle="tab" class="bu_icon">BUSINESS</a>
					</li>
					<li>
						<a href="#3a" data-toggle="tab" class="ar_icon">ART & HUMANITIES</a>
					</li>
				</ul>
			</div>
		</div>
		
	</div>
</div>

<div id="exTab1" class="faculty_member_tab_contain">	
	<div class="container">
		<div class="tab-content clearfix">
			<div class="tab-pane active" id="1a">
	      		<div class="faculty_member_tab_header">
	      			<div class="faculty_member_title">
		      			<h5><?php the_field('faculty_member_title'); ?></h5>
		      			<h1><?php the_field('faculty_member_main'); ?></h1>
						<p><?php the_field('faculty_member_sub-title'); ?></p>
		      		</div>
		      		<div class="faculty_member_cat">
		      			<div class="faculty_member_cat_inner">
							<h5>DEPARTMENT OF</h5>
						
							<!-- <?php
							$args = array('parent' => 7);
							$categories = get_categories( $args );
							foreach($categories as $category) { 
							    echo '<p>Category: <a href="' . get_category_link( $category->term_id ) . '" title="' . sprintf( __( "View all posts in %s" ), $category->name ) . '" ' . '>' . $category->name.'</a> </p> ';
							}?> -->
						</div> 

		      		</div>
	      		</div>
	      		<div class="faculty_member_list">
	      			<div class="row">
						<?php   
					  $loop = new WP_Query( array( 'post_type' => 'faculty-member', 'posts_per_page' =>'6','order'=>'Des') ); 
					   while ( $loop->have_posts() ) : $loop->the_post(); ?>
					   <div class="col-md-6 col-sm-6 col-xs-12">
					   		<div class="faculty_member_item">
					   			<div class="faculty_member_item_img">
					   				<?php the_post_thumbnail() ?>
					   			</div>
					   			<div class="faculty_member_item_text">
					   				<h1><a href="#<?php the_ID();?>" class=""><?php the_title(); ?></a> </h1>
					   				<div class="faculty_member_item_content">
					   					<?php the_content(); ?>
					   					<h3><?php the_field('employer_department'); ?></h3>
					   					<h5><?php the_field('lecturer'); ?></h5>
					   					<h4 class="mobile"><?php the_field('employer_mobile'); ?></h4>
					   					<h4 class="email"><?php the_field('employer_email'); ?></h4>
					   					<div class="employer_interested">
					   						<span>Interested area |</span><?php the_field('employer_interested'); ?>
					   					</div>
					   				</div>
					   				
					   			</div>
					   		</div>
						 </div>
					    <?php endwhile; ?>
					 </div>	
	      		</div>
			</div>
			
			<div class="tab-pane" id="2a">
	      		<div class="faculty_member_tab_header">
	      			<div class="faculty_member_title">
		      			<h5><?php the_field('faculty_member_title'); ?></h5>
		      			<h1><?php the_field('faculty_member_main'); ?></h1>
						<p><?php the_field('faculty_member_sub-title'); ?></p>
		      		</div>
		      		<div class="faculty_member_cat">
		      			<div class="faculty_member_cat_inner">
							<h5>DEPARTMENT OF</h5>
							<ul> 
							<?php
							$args = array('parent' => 8);
							$categories = get_categories( $args );
							foreach($categories as $category) { 
							    echo '<p>Category: <a href="' . get_category_link( $category->term_id ) . '" title="' . sprintf( __( "View all posts in %s" ), $category->name ) . '" ' . '>' . $category->name.'</a> </p> ';
							}?>
						</div> 
						
		      		</div>
	      		</div>
	      		<div class="faculty_member_list">
	      			<div class="row">
						<?php   
					  $loop = new WP_Query( array( 'post_type' => 'faculty-member', 'posts_per_page' =>'6','order'=>'Des') ); 
					   while ( $loop->have_posts() ) : $loop->the_post(); ?>
					   <div class="col-md-6 col-sm-6 col-xs-12">
					   		<div class="faculty_member_item">
					   			<div class="faculty_member_item_img">
					   				<?php the_post_thumbnail() ?>
					   			</div>
					   			<div class="faculty_member_item_text">
					   				<h1><a href="#<?php the_ID();?>" class=""><?php the_title(); ?></a> </h1>
					   				<div class="faculty_member_item_content">
					   					<?php the_content(); ?>
					   					<h3><?php the_field('employer_department'); ?></h3>
					   					<h5><?php the_field('lecturer'); ?></h5>
					   					<h4 class="mobile"><?php the_field('employer_mobile'); ?></h4>
					   					<h4 class="email"><?php the_field('employer_email'); ?></h4>
					   					<div class="employer_interested">
					   						<span>Interested area |</span><?php the_field('employer_interested'); ?>
					   					</div>
					   				</div>
					   				
					   			</div>
					   		</div>
						 </div>
					    <?php endwhile; ?>
					 </div>	
	      		</div>
			</div>
	    
	    	<div class="tab-pane" id="3a">
	      		<div class="faculty_member_tab_header">
	      			<div class="faculty_member_title">
		      			<h5><?php the_field('faculty_member_title'); ?></h5>
		      			<h1><?php the_field('faculty_member_main'); ?></h1>
						<p><?php the_field('faculty_member_sub-title'); ?></p>
		      		</div>
		      		<div class="faculty_member_cat">
		      			<div class="faculty_member_cat_inner">
							<h5>DEPARTMENT OF</h5>
							<ul> 
							<?php
							$args = array('parent' => 9);
							$categories = get_categories( $args );
							foreach($categories as $category) { 
							    echo '<p>Category: <a href="' . get_category_link( $category->term_id ) . '" title="' . sprintf( __( "View all posts in %s" ), $category->name ) . '" ' . '>' . $category->name.'</a> </p> ';
							}?>
						</div> 
						
		      		</div>
	      		</div>
	      		<div class="faculty_member_list">
	      			<div class="row">
						<?php   
					  $loop = new WP_Query( array( 'post_type' => 'faculty-member', 'posts_per_page' =>'6','order'=>'Des') ); 
					   while ( $loop->have_posts() ) : $loop->the_post(); ?>
					   <div class="col-md-6 col-sm-6 col-xs-12">
					   		<div class="faculty_member_item">
					   			<div class="faculty_member_item_img">
					   				<?php the_post_thumbnail() ?>
					   			</div>
					   			<div class="faculty_member_item_text">
					   				<h1><a href="#<?php the_ID();?>" class=""><?php the_title(); ?></a> </h1>
					   				<div class="faculty_member_item_content">
					   					<?php the_content(); ?>
					   					<h3><?php the_field('employer_department'); ?></h3>
					   					<h5><?php the_field('lecturer'); ?></h5>
					   					<h4 class="mobile"><?php the_field('employer_mobile'); ?></h4>
					   					<h4 class="email"><?php the_field('employer_email'); ?></h4>
					   					<div class="employer_interested">
					   						<span>Interested area |</span><?php the_field('employer_interested'); ?>
					   					</div>
					   				</div>
					   				
					   			</div>
					   		</div>
						 </div>
					    <?php endwhile; ?>
					 </div>	
	      		</div>
			</div>
		</div>
	</div>
</div>

<div class="mobile_tab">
<div class="faculty_member_tab">
	<ul  class="tabs">
		<li class="active" rel="tab1"><a  class="sc_icon">SCIENCE & ENGINEERING</a></li>
		<li class="second_t" rel="tab2">
			<a class="bu_icon">BUSINESS</a>
		</li>
		<li rel="tab3">
			<a class="ar_icon">ART & HUMANITIES</a>
		</li>
	</ul>
</div>
<div  class="faculty_member_tab_contain tab_container">	
	<div class="container">
		<div class="tab-content clearfix">
		 <h3 class="d_active tab_drawer_heading" rel="tab1"><a  class="sc_icon">SCIENCE & ENGINEERING</a></h3>
		<div class="tab-pane tab_content" id="tab1">
      		<div class="faculty_member_tab_header">
      			<div class="faculty_member_title">
	      			<h5><?php the_field('faculty_member_title'); ?></h5>
	      			<h1><?php the_field('faculty_member_main'); ?></h1>
					<p><?php the_field('faculty_member_sub-title'); ?></p>
	      		</div>
	      		<div class="faculty_member_cat">
	      			<div class="faculty_member_cat_inner">
						<h5>DEPARTMENT OF</h5>
						<ul> 
						<?php
						$args = array('parent' => 8);
						$categories = get_categories( $args );
						foreach($categories as $category) { 
						    echo '<p>Category: <a href="' . get_category_link( $category->term_id ) . '" title="' . sprintf( __( "View all posts in %s" ), $category->name ) . '" ' . '>' . $category->name.'</a> </p> ';
						}?>
					</div> 
					
	      		</div>
      		</div>
      		<div class="faculty_member_list">
      			<div class="row">
					<?php   
				  $loop = new WP_Query( array( 'post_type' => 'faculty-member', 'posts_per_page' =>'6','order'=>'Des') ); 
				   while ( $loop->have_posts() ) : $loop->the_post(); ?>
				   <div class="col-md-6 col-sm-6 col-xs-12">
				   		<div class="faculty_member_item">
				   			<div class="faculty_member_item_img">
				   				<?php the_post_thumbnail() ?>
				   			</div>
				   			<div class="faculty_member_item_text">
				   				<h1><a href="#<?php the_ID();?>" class=""><?php the_title(); ?></a> </h1>
				   				<div class="faculty_member_item_content">
				   					<?php the_content(); ?>
				   					<h3><?php the_field('employer_department'); ?></h3>
				   					<h5><?php the_field('lecturer'); ?></h5>
				   					<h4 class="mobile"><?php the_field('employer_mobile'); ?></h4>
				   					<h4 class="email"><?php the_field('employer_email'); ?></h4>
				   					<div class="employer_interested">
				   						<span>Interested area |</span><?php the_field('employer_interested'); ?>
				   					</div>
				   				</div>
				   				
				   			</div>
				   		</div>
					 </div>
				    <?php endwhile; ?>
				 </div>	
      		</div>
		</div>

		 <h3 class="tab_drawer_heading" rel="tab2"><a  class="sc_icon">BUSINESS</a></h3>
			<div class="tab-pane tab_content" id="tab2">
	      		<div class="faculty_member_tab_header">
	      			<div class="faculty_member_title">
		      			<h5><?php the_field('faculty_member_title'); ?></h5>
		      			<h1><?php the_field('faculty_member_main'); ?></h1>
						<p><?php the_field('faculty_member_sub-title'); ?></p>
		      		</div>
		      		<div class="faculty_member_cat">
		      			<div class="faculty_member_cat_inner">
							<h5>DEPARTMENT OF</h5>
							<ul> 
							<?php
							$args = array('parent' => 8);
							$categories = get_categories( $args );
							foreach($categories as $category) { 
							    echo '<p>Category: <a href="' . get_category_link( $category->term_id ) . '" title="' . sprintf( __( "View all posts in %s" ), $category->name ) . '" ' . '>' . $category->name.'</a> </p> ';
							}?>
						</div> 
						
		      		</div>
	      		</div>
	      		<div class="faculty_member_list">
	      			<div class="row">
						<?php   
					  $loop = new WP_Query( array( 'post_type' => 'faculty-member', 'posts_per_page' =>'6','order'=>'Des') ); 
					   while ( $loop->have_posts() ) : $loop->the_post(); ?>
					   <div class="col-md-6 col-sm-6 col-xs-12">
					   		<div class="faculty_member_item">
					   			<div class="faculty_member_item_img">
					   				<?php the_post_thumbnail() ?>
					   			</div>
					   			<div class="faculty_member_item_text">
					   				<h1><a href="#<?php the_ID();?>" class=""><?php the_title(); ?></a> </h1>
					   				<div class="faculty_member_item_content">
					   					<?php the_content(); ?>
					   					<h3><?php the_field('employer_department'); ?></h3>
					   					<h5><?php the_field('lecturer'); ?></h5>
					   					<h4 class="mobile"><?php the_field('employer_mobile'); ?></h4>
					   					<h4 class="email"><?php the_field('employer_email'); ?></h4>
					   					<div class="employer_interested">
					   						<span>Interested area |</span><?php the_field('employer_interested'); ?>
					   					</div>
					   				</div>
					   				
					   			</div>
					   		</div>
						 </div>
					    <?php endwhile; ?>
					 </div>	
	      		</div>
			</div>

			 <h3 class="tab_drawer_heading" rel="tab3"><a  class="sc_icon">ART & HUMANITIES</a></h3>
			<div class="tab-pane tab_content" id="tab3">
	      		<div class="faculty_member_tab_header">
	      			<div class="faculty_member_title">
		      			<h5><?php the_field('faculty_member_title'); ?></h5>
		      			<h1><?php the_field('faculty_member_main'); ?></h1>
						<p><?php the_field('faculty_member_sub-title'); ?></p>
		      		</div>
		      		<div class="faculty_member_cat">
		      			<div class="faculty_member_cat_inner">
							<h5>DEPARTMENT OF</h5>
							<ul> 
							<?php
							$args = array('parent' => 8);
							$categories = get_categories( $args );
							foreach($categories as $category) { 
							    echo '<p>Category: <a href="' . get_category_link( $category->term_id ) . '" title="' . sprintf( __( "View all posts in %s" ), $category->name ) . '" ' . '>' . $category->name.'</a> </p> ';
							}?>
						</div> 
						
		      		</div>
	      		</div>
	      		<div class="faculty_member_list">
	      			<div class="row">
						<?php   
					  $loop = new WP_Query( array( 'post_type' => 'faculty-member', 'posts_per_page' =>'6','order'=>'Des') ); 
					   while ( $loop->have_posts() ) : $loop->the_post(); ?>
					   <div class="col-md-6 col-sm-6 col-xs-12">
					   		<div class="faculty_member_item">
					   			<div class="faculty_member_item_img">
					   				<?php the_post_thumbnail() ?>
					   			</div>
					   			<div class="faculty_member_item_text">
					   				<h1><a href="#<?php the_ID();?>" class=""><?php the_title(); ?></a> </h1>
					   				<div class="faculty_member_item_content">
					   					<?php the_content(); ?>
					   					<h3><?php the_field('employer_department'); ?></h3>
					   					<h5><?php the_field('lecturer'); ?></h5>
					   					<h4 class="mobile"><?php the_field('employer_mobile'); ?></h4>
					   					<h4 class="email"><?php the_field('employer_email'); ?></h4>
					   					<div class="employer_interested">
					   						<span>Interested area |</span><?php the_field('employer_interested'); ?>
					   					</div>
					   				</div>
					   				
					   			</div>
					   		</div>
						 </div>
					    <?php endwhile; ?>
					 </div>	
	      		</div>
			</div>
 
</div>
</div>
</div>
</div>
<!-- .tab_container -->
</div>
</div>
	<div class="container">
		
	</div>



	<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();

			// Include the page content template.
			get_template_part( 'template-parts/content', 'page' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) {
				comments_template();
			}

			// End of the loop.
		endwhile;
		?>

	</main><!-- .site-main -->
	


	<?php get_sidebar( 'content-bottom' ); ?>

</div><!-- .content-area -->



	<div id="primary" class="site-content">
		<div id="content" role="main">
			<div class="container">

			</div>
		</div><!-- #content -->
			</div>
	</div><!-- #primary -->


<script type="text/javascript">
	function faculty_list() 
	{
		console.log("calledd");
	}
</script>
<script type="text/javascript">
     // tabbed content
        // http://www.entheosweb.com/tutorials/css/tabs.asp
        $(".tab_content").hide();
        $(".tab_content:first").show();

      /* if in tab mode */
        $("ul.tabs li").click(function() {
            
          $(".tab_content").hide();
          var activeTab = $(this).attr("rel"); 
          $("#"+activeTab).fadeIn();        
            
          $("ul.tabs li").removeClass("active");
          $(this).addClass("active");

          $(".tab_drawer_heading").removeClass("d_active");
          $(".tab_drawer_heading[rel^='"+activeTab+"']").addClass("d_active");
          
        });
        /* if in drawer mode */
        $(".tab_drawer_heading").click(function() {
          
          $(".tab_content").hide();
          var d_activeTab = $(this).attr("rel"); 
          $("#"+d_activeTab).fadeIn();
          
          $(".tab_drawer_heading").removeClass("d_active");
          $(this).addClass("d_active");
          
          $("ul.tabs li").removeClass("active");
          $("ul.tabs li[rel^='"+d_activeTab+"']").addClass("active");
        });
        
        
        /* Extra class "tab_last" 
           to add border to right side
           of last tab */
        $('ul.tabs li').last().addClass("tab_last");
        
</script>
<?php get_sidebar(); ?>
<?php get_footer(); ?>

