<?php
/**
 * The Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>
<div class="page_banner">
	<div class="banner_inner">
		<?php if (get_field('banner_image')) {
		?>
			<img src="<?php the_field('banner_image2'); ?>">
		<?php
		}
		else{
		?>
			<img src=" <?php the_field('banner_images', 'option'); ?>">	 
		<?php	
		}
		?>
		<div class="container">
			<div class="caption">
				<div class="bannerfirst_title-outer"><div class="bannerfirst_title"><?php the_field('post_header_title_first', 'option'); ?></div></div>
				<div class="bannersecond_title_outer"><div class="bannersecond_title"><?php the_field('post_header_title_second', 'option'); ?></div></div>
				<div class="bannersub_title_outer"><div class="bannersub_title"><?php the_field('post_header_sub_title', 'option'); ?></div></div>
				<div class="bannerextra_content"><?php the_field('add_extra_content'); ?></div>
			</div>
		</div>
		<div class="category-select-main">
			<div class="category-select-inner">
				<form id="category-select" class="category-select" action="<?php echo esc_url( home_url( '/' ) ); ?>" method="get">
					<div class="category-item">
						<label>FACULTY OF</label>
						<?php wp_dropdown_categories( 'hierarchical=1&parent=0' ); ?> 
					</div>
					<div class="category-item">
						<label>DEPARTMENT OF</label>
						<?php wp_dropdown_categories( 'parent_item=null' ); ?>
					</div>
					<div class="category-item">
						<label>CHOOSE COURSES</label>
						<?php
						// Function that returns post titles from specific post type as form select element
						// returns null if found no results.

						function output_projects_list() {
						    global $wpdb;

						    $custom_post_type = 'post'; // define your custom post type slug here

						    // A sql query to return all post titles
						    $results = $wpdb->get_results( $wpdb->prepare( "SELECT ID, post_title FROM {$wpdb->posts} WHERE post_type = %s and post_status = 'publish'", $custom_post_type ), ARRAY_A );

						    // Return null if we found no results
						    if ( ! $results )
						        return;

						    // HTML for our select printing post titles as loop
						    $output = '<select name="project" id="project">';

						    foreach( $results as $index => $post ) {
						        $output .= '<option value="' . $post['ID'] . '">' . $post['post_title'] . '</option>';
						    }

						    $output .= '</select>'; // end of select element

						    // get the html
						    return $output;
						}

						// Then in your project just call the function
						// Where you want the select form to appear
						echo output_projects_list();
						?>
					</div>
						
						<input type="submit" name="Submit" value="Submit" />
				</form>
			</div>
	</div>
	</div>
	
</div>


<div class="container">
	<div class="caption">
		<div class="bannerfirst_title-outer"><div class="bannerfirst_title"><?php the_field('left aligned'); ?></div></div>
	</div>
</div>
<div class="container">
<div class="row">	
<div class="post_tabs">
<div class="tab_div">	
<?php 
  $tabSection = 0;
  $tabLabel = 0;
  $tabContent = 0;
?>
<section class="tab-section tab-section--<?php echo $tabSection++; ?>">
  <nav class="tab-nav">
   <?php if(have_rows('tabs')): ?>
    <?php while(have_rows('tabs') ) : the_row(); ?>
     <span><a href="#tab-content--<?php echo $tabLabel++; ?>"><?php  the_sub_field('tab_label'); ?></a></span>
    <?php endwhile; ?>
   <?php endif; ?>
  </nav>

  <div class="tabs_c_contain">
	  <?php if(have_rows('tabs')): ?>
	    <?php while(have_rows('tabs') ) : the_row(); ?>
	     	<div id="tab-content--<?php echo $tabContent++; ?>" class="tab-content"><?php the_sub_field('tab_content'); ?></div>
	    <?php endwhile; ?>
	  <?php endif; ?>
  </div>

  <div class="apply_div">	
		<a href="<?php the_field('button-url'); ?>"><?php the_field('button-name'); ?></a>
		<?php the_field('duration'); ?>
		<?php the_field('acedemic'); ?>
		<?php the_field('campus'); ?>
	</div>
</section>
</div>

<div class="post_last_section">	
<?php  
if( have_rows('post-contect') ):

    while( have_rows('post-contect') ) : the_row(); 
        
        ?>
        <div class="post_last_section_item">
	        <div class="post_last_section_text">
	        	<span><?php the_sub_field('post_content'); ?></span>
	        </div>
	        <div class="post_last_section_img">
	        	<img src="<?php the_sub_field('post-image'); ?>">
	        </div>
        </div>
		<?php
			
    endwhile;

endif;
?>
</div> 
</div>
</div>
</div>
<div class="success_div" style="background-image: url('<?php the_field('background-image', 'option'); ?>');">
	<div class="container">	
		<div class="success_div_title">
			<?php the_field('title-1', 'option'); ?>
			<?php the_field('title-2', 'option'); ?>
		</div>
		<div class="success_div_slider">
			<?php echo do_shortcode('[wonderplugin_carousel id="3"]'); ?>
		</div>
	</div>
</div>
<div id="primary" class="site-content">
	<div id="content" role="main">
		<div class="container">
		

	</div><!-- #content -->
		</div>
</div><!-- #primary -->
<?php get_sidebar(); ?>
<?php get_footer(); ?>