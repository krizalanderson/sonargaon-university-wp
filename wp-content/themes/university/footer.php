<?php
    /**
     * The template for displaying the footer
     *
     * Contains footer content and the closing of the #main and #page div elements.
     *
     * @package WordPress
     * @subpackage Twenty_Twelve
     * @since Twenty Twelve 1.0
     */
    ?>
</div><!-- #main .wrapper -->
<footer>
    <div class="footer_outer">
        <div class="footer_inner">
            <div class="container">
                <div class="footer_top">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="footer1">
                                <?php dynamic_sidebar('footer1'); ?>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="footer2">
                                <?php dynamic_sidebar('footer2'); ?>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="footer3">
                                <hr>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer_middle">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="footer4">
                                <?php dynamic_sidebar('footer3'); ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="footer5">
                                <?php dynamic_sidebar('footer4'); ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="footer6"> 
                                <?php dynamic_sidebar('footer5'); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <hr class="footer_seprater">
                <div class="footer_bottom">
                    <div class="row">
                        <div class="col-md-3 col-sm-4">
                            <div class="footer_link1">
                                <?php dynamic_sidebar('footer6'); ?>
                            </div>
                        </div>
                        <div class="col-md-3  col-sm-5">
                            <div class="footer_link2">      
                                <h3 class="widget-title">follow us</h3>                
                                <?php if( have_rows('social_links', 'option') ): ?>

                                    <ul class="social-media-icons">

                                    <?php while( have_rows('social_links', 'option') ): the_row(); ?>

                                        <li><a href="<?php the_sub_field('link'); ?>" data-placement="bottom" data-toggle="tooltip" title="<?php the_sub_field('name'); ?>"><img src="<?php the_sub_field('image'); ?>"></a></li>

                                    <?php endwhile; ?>

                                    </ul>

                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="col-md-4  col-sm-1">
                            <div class="footer_link3">
                            </div>
                        </div>
                        <div class="col-md-2  col-sm-2">
                            <div class="footer_link4"> 
                                <img src="<?php the_field('footer_logo', 'option'); ?>" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="footer_menu">
                        <?php wp_nav_menu( array(                           
                            'menu' => 'Footer Menu',
                            'items_wrap' => '<ul class="footermenu">%3$s</ul>'
                            ) ); 
                        ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="copyright_text">
                        <?php the_field('copyright_text', 'option'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- #colophon -->
<?php wp_footer(); ?>
</body>
</html>