<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?>
<div class="page_banner">
	<div class="banner_inner">
		
		<?php if (get_field('banner_image')) {
		?>
			<img src="<?php the_field('banner_image'); ?>">
		<?php
		}
		else{
		?>
			<img src="<?php echo home_url(); ?>/wp-content/uploads/2017/05/defaultbaneer.jpg">
		<?php	
		}
		?>

		<div class="container">
			<div class="caption">
				<div class="bannerfirst_title-outer"><div class="bannerfirst_title"><?php the_field('banner_title_first'); ?></div></div>
				<div class="bannersecond_title_outer"><div class="bannersecond_title"><?php the_field('banner_title_second'); ?></div></div>
				<div class="bannersub_title_outer"><div class="bannersub_title"><?php the_field('banner_sub_title'); ?></div></div>
				<div class="bannerextra_content"><?php the_field('add_extra_content'); ?></div>
			</div>
		</div>
		<div class="letest_event">
			<div class="letest_event_inner">
				<?php the_field('letest_event'); ?>
			</div>
		</div>
		
	</div>
</div>

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="entry-content">
			<div class="container">
				<?php the_content(); ?>
				<?php wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'twentytwelve' ), 'after' => '</div>' ) ); ?>
			</div>
		</div><!-- .entry-content -->
	</article><!-- #post -->
