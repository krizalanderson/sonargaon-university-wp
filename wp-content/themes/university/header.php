<?php
    /**
     * The Header template for our theme
     *
     * Displays all of the <head> section and everything up till <div id="main">
     *
     * @package WordPress
     * @subpackage Twenty_Twelve
     * @since Twenty Twelve 1.0
     */
    ?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
    <![endif]-->
    <!--[if IE 8]>
    <html class="ie ie8" <?php language_attributes(); ?>>
        <![endif]-->
        <!--[if !(IE 7) & !(IE 8)]><!-->
        <html <?php language_attributes(); ?>>
            <!--<![endif]-->
            <head>
                <meta charset="<?php bloginfo( 'charset' ); ?>" />
                <meta name="viewport" content="width=device-width" />
                <title><?php wp_title( '|', true, 'right' ); ?></title>
                <link rel="profile" href="http://gmpg.org/xfn/11" />
                <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
                <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/vendor/bootstrap.min.css">
                <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/vendor/font-awesome.min.css">
                <script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/js/vendor/jquery-1.12.3.min.js'></script>
                <script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/js/vendor/bootstrap.min.js'></script>
                <script>
                $(document).ready(function(){
                    $('[data-toggle="tooltip"]').tooltip();   
                    $('.tab-section').each(function(i){
                        $('.tab-section--' + i + ' .tab-content').not(':first').hide();
                        $('.tab-section--' + i + ' .tab-content:first').show();
                        $('.tab-section--' + i + ' .tab-nav span:first').addClass('tab-nav--active');
                        $('.tab-section--' + i + ' .tab-nav').on('click', 'span', function(e){
                            var index = $(this).index();
                            $('.tab-section--' + i + ' .tab-nav span.tab-nav--active').removeClass('tab-nav--active');
                            $(this).addClass('tab-nav--active');
                            $('.tab-section--' + i + ' .tab-content').hide();
                            $('#tab-content--'+ index ).show();
                            //console.log(document.getElementById('tab-content--'+ index));
                            $($('a',this).attr('href')).show();
                            return false;
                        });
                    });
                    $( ".homesecfour .vc_tta-tab  a span" ).before( "<div class='tablebel'>Faculty Of</div>" );
                    $( ".homesecfour.aboutsecfour .vc_tta-tab  a span" ).before( "<div class='tablebel'>SONARGAON</div>" );
                    $( ".homesecfour  .vc_tta-panel-body .col-md-3:nth-child(4n)" ).after( "<div class='clt-spacer'></div>" );
                    $( ".categorysecone .vc_tta-tab  a span" ).before( "<div class='tablebel'>Faculty Of</div>" );
                    $( ".categorysecone  .vc_tta-panel-body .col-md-3:nth-child(4n)" ).after( "<div class='clt-spacer'></div>" );
                    $( ".categorysectwo  .graduateprogram .col-md-4:nth-child(3n)" ).after( "<div class='clt-spacer'></div>" );
                    $( ".categorysecthree .col-md-3:nth-child(4n)" ).after( "<div class='clt-spacer'></div>" );
                    $( ".homesecfiveleft .vc_gitem-animated-block" ).removeClass( "vc_gitem-animate-blurScaleOut" );
                });
                </script>
                <script type="text/javascript">
                    $(document).ready(function(){
                        $('#be-click li').click(function(){
                            console.log('your message');
                           $('#'+$(this).data('id')).slideToggle("slow"); 
                        });

                    });
                </script>
                               <?php // Loads HTML5 JavaScript file to add support for HTML5 elements in older IE versions. ?>
                <!--[if lt IE 9]>
                <script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
                <![endif]-->
                <?php wp_head(); ?>
            </head>
            <body <?php body_class(); ?>>
                <header class="site-header">
                    <div class="header_top">
                        <div class="container">
                            <div id="search-container" class="search-box-wrapper pull-right">
                                <div class="opal-dropdow-search dropdown">
                                    <a data-target=".bs-search-modal-lg" data-toggle="modal" class="btn btn-link search-focus dropdown-toggle searchbutton dropdown-toggle-overlay">
                                    	<i class="fa fa-search"></i>
                                    </a>
                                    <div class="modal fade bs-search-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button aria-label="Close" data-dismiss="modal" class="close btn btn-sm btn-primary pull-right" type="button"><span aria-hidden="true">x</span></button>
                                                    <h4 id="gridSystemModalLabel" class="modal-title">Search</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="toggle-overlay-container">
                                                        <div class="search-box">
                                                            <form method="get" class="input-group search-category" action="<?php echo home_url( '/' ); ?>">
                                                                <input type="search" maxlength="60" class="form-control search-category-input" class="search-field" placeholder="<?php echo esc_attr_x( 'Search …', 'placeholder' ) ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
                                                                <button type="submit" class="btn btn-primary btn-search"><i class="fa fa-search"></i></button>
                                                            </form>
                                                        </div>
                                                        <div class="dropdown-toggle-button" data-target=".toggle-overlay-container"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="header_top_inner">
                                <?php wp_nav_menu( array(	 						
                                    'menu' => 'Top_Menu',
                                    'items_wrap' => '<ul class="headtopmenu">%3$s</ul>'
                                    ) ); 
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="header_bottom">
                        <div class="container">
                            <div class="header_bottom_inner">
                                <div class="row">
                                    <div class="col-sm-2 col-md-2">
                                        <div class="site_logo">
                                            <a href="<?php echo home_url(); ?>">
                                                <img class="logo logo-light" alt="Logo" src="<?php the_field('header_logo', 'option'); ?>">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-sm-10 col-md-10">
                                        <?php wp_nav_menu( array(
                                            'menu' => 'Menu 1',
                                            'items_wrap' => '<ul class="headbottommenu">%3$s</ul>'
                                            ) ); 
                                            ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
                <!-- #masthead -->
                <div id="main" class="wrapper">